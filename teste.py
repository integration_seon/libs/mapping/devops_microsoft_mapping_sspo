from devops_microsoft_mapping_sspo import factories 
from tfs.tfs import TFS

organization = "e2b9d512bc4743c7ae0daaa251636fa7"
tfs_key = "l4it6lw7fj63qptaqfwm4tnj3fk4yxis7gczk6cenobrzx62f73q"
tfs_url = "https://dev.azure.com/paulossjunior/"
application = "8cd02797eb70467d81066ff874cd942d"

content = {
    'createdDate': '2019-11-29T21:11:46.8222058Z',
    'eventType': 'workitem.created',
    'id': '69',
    'resourceContainers': 
        {
            'account': {'baseUrl': 'https://dev.azure.com/paulossjunior/','id': '16ad67c1-3286-44bb-a0bb-d8ef245f21d2'},
            'collection': {'baseUrl': 'https://dev.azure.com/paulossjunior/','id': '31d31367-7618-485a-a500-f2dcb12e14ec'},
            'project': {'baseUrl': 'https://dev.azure.com/paulossjunior/','id': '0cfe9a70-c6ce-45b0-a898-d3594fc6a08a'}
        }

    }

data = {'organization_id': organization, 
                "tfs_key": tfs_key, 
                "tfs_url": tfs_url,
                "application": application, 
                "content": content
        }  
tfs =  TFS(tfs_key, tfs_url) 

#element = tfs.get_work_item(69, None,None, "All")
#x = factories.ScrumIntentedDevelopmentTaskFactory()
#i = x.create(element)
#x = factories.ScrumPerformedDevelopmentTaskFactory()
#x.create(element, i)

#projects = tfs.get_projects()
#for project in projects: 
#    x = factories.ScrumAtomicProjectFactory()
#    x.create(project, organization)

element = tfs.get_work_item(74, None,None, "All")
x = factories.AtomicUserStoryFactory()
i = x.create(element)

element = tfs.get_work_item(72, None,None, "All")
x = factories.EpicFactory()
i = x.create(element)